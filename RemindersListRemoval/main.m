//
//  main.m
//  RemindersListRemoval
//
//  Created by Henrique Galo on 5/29/14.
//  Copyright (c) 2014 Henrique Galo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
