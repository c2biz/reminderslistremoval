//
//  AppDelegate.h
//  RemindersListRemoval
//
//  Created by Henrique Galo on 5/29/14.
//  Copyright (c) 2014 Henrique Galo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
