//
//  TableViewController.h
//  RemindersListRemoval
//
//  Created by Henrique Galo on 5/29/14.
//  Copyright (c) 2014 Henrique Galo. All rights reserved.
//

#import <UIKit/UIKit.h>
@import EventKit;
@interface TableViewController : UITableViewController
@property (nonatomic, retain) EKEventStore *store;
@end
